<?php

/**
 * Api_Model_Base_UserBookmark
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $bookmark_id
 * @property integer $user_id
 * @property datetime $created_at
 * @property Api_Model_User $User
 * @property Api_Model_Bookmark $Bookmark
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class Api_Model_Base_UserBookmark extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('bookmarks_users');
        $this->hasColumn('bookmark_id', 'string', 36, array(
             'type' => 'string',
             'primary' => true,
             'length' => '36',
             ));
        $this->hasColumn('user_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('created_at', 'datetime', null, array(
             'type' => 'datetime',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Api_Model_User as User', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $this->hasOne('Api_Model_Bookmark as Bookmark', array(
             'local' => 'bookmark_id',
             'foreign' => 'id'));
    }
}