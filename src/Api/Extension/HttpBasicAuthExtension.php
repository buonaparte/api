<?php

class Api_Extension_HttpBasicAuthExtension implements Turtle_ExtensionInterface
{
    public function extend(Turtle_Application $app)
    {
        if (! isset($app['http_basic_auth.realm'])) {
            $app['http_basic_auth.realm'] = 'Needs Auth';
        }

        $app->onRequest(array($this, 'onRequest'));
    } 

    public function onRequest(Turtle_Application $app, Turtle_Component_Http_Request $request)
    {
        $username = $request->getUser();
        if (! $user = $app['db.em']('Api_Model_User')->findOneByUsername($username ? $username : '')
            or ! $user->checkPassword($request->getPassword())
        ) {
            // Turtle_Component_Http_HttpException_UnauthorizedHttpException + Realm
            $app->abort(401, null, array(
                'WWW-Authenticate' => sprintf('Basic realm="%s"', $app['http_basic_auth.realm'])
            ));
        }

        // not authorized
        if (! $user->isActive()) {
            throw new Turtle_Component_Http_HttpException_ForbiddenHttpException(null);
        }

        // all good
        $app['user'] = $user;
    }
}