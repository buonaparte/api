<?php

class Api_Extension_SerializerExtension implements Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app)
	{
		$app['serializer.default_format'] = 'xml';

		if (! isset($app['serializer.fallback_format'])) {
			$app['serializer.fallback_format'] = 'xml';
		}

		$app['serializer'] = $app->share(array($this, 'loadSerializer'));
		$app->extend('serializer', array($this, 'restrictFormat'));

		$app->onRequest(array($this, 'onRequest'));
	}

	public function loadSerializer(Turtle_Application $app)
	{
		$serializer = new Turtle_Component_Serializer_Serializer();
		// loads
		$serializer->addLoader('xml', new Turtle_Component_Serializer_Loader_XmlLoader());
		$serializer->addLoader('json', new Turtle_Component_Serializer_Loader_JsonLoader());
		// dumps
		$serializer->addDumper('xml', new Turtle_Component_Serializer_Dumper_XmlDumper());
		$serializer->addDumper('json', new Turtle_Component_Serializer_Dumper_JsonDumper());

		return $serializer;
	}

	public function restrictFormat($serializer, Turtle_Application $app)
	{
		if (! $serializer->dumps($app['serializer.fallback_format']) 
			|| ! $serializer->loads($app['serializer.fallback_format'])) {
			$app['serializer.fallback_format'] = $app['serializer.default_format'];
		} 

		return $serializer;
	}

	public function onRequest(Turtle_Application $app, Turtle_Component_Http_Request $request)
	{
		if (! $app['serializer']->loads($format = $request->headers->get('x_format', $request->query->get('format', $app['serializer.default_format'])))) {
			$app['serializer.format'] = $app['serializer.fallback_format'];
		} else {
			$app['serializer.format'] = $format;
		}
	}
}