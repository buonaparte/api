<?php

require __DIR__.'/../vendor/autoload.php';

$client = new Turtle_Component_HttpClient_Client('http://localhost/api/web/index.php');
$client->sync(false);

/* POST test */
// $client->doPost('/', array('X-Format' => 'json'), array(), json_encode(array(
// 	'name'        => 'Test client',
// 	'url'         => 'youtube.com',
// 	'description' => 'description test'
// )))->setCredentials('denis', '123456');

/* PUT test */
// $client->doPut('/850ac84e52661e1eec1f53bf1eeb53bf', array('X-Format' => 'json'), array(), json_encode(array(
// 	'name' => 'Updated name'
// )))->setCredentials('denis', '123456');

/* DELETE test */
$client->doDelete('/850ac84e52661e1eec1f53bf1eeb53bf', array('X-Format' => 'json'))
	->setCredentials('denis', '123456');

$context = $client->send();
var_dump($context[0]);