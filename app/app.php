<?php

$app = require __DIR__.'/bootstrap.php';

$bookmarkConverter = function (Turtle_Application $app, $bookmark) {
	return $app['db.em']('Api_Model_Bookmark')->findOneById($bookmark);
};

$app->get('/', function (Turtle_Application $app) {
	$data = $app['db.em']('Api_Model_Bookmark')->createQuery('b')
		->select('b.id, b.url, b.name, b.description, b.updated_at')
		->where('b.owner_id = ?', $app['user']->id)
		->orderBy('b.updated_at desc')
		->limit(10)
		->execute(array(), Doctrine_Core::HYDRATE_ARRAY);

	return $app->respond(
		$app['serializer']->getDumper($app['serializer.format'])->dump(array(
			'recentBookmarks' => array('bookmark' => $data)
		)),
		// no-content ?
		empty($data) ? 204 : 200,
		array(
			'content_type' => $app['request']->getMimeType($app['serializer.format']),
			'x_format'     => $app['serializer.format']
		)
	);
});

$app->post('/', function (Turtle_Application $app, Turtle_Component_Http_Request $request) {
	try {
		$data = @$app['serializer']->getLoader($app['serializer.format'])->load($request->body);

		$bookmark = $app['db.em']('Api_Model_Bookmark')->create();
		foreach ((array) $data as $k => $v) {
			$bookmark->$k = $v;
		}
		// we know the Owner
		$bookmark->Owner = $app['user'];
		$bookmark->save();
	} catch (Exception $e) {
		$app->abort(
			400, 
			$app['serializer']->getDumper($app['serializer.format'])->dump(array('error' => $e->getMessage())), 
			array('content_type' => $request->getMimeType($app['serializer.format']))
		);
	}

	return $app->respond(
		$app['serializer']->getDumper($app['serializer.format'])->dump(array('bookmark' => $bookmark->toArray(false))),
		201,
		array('content_type' => $request->getMimeType($app['serializer.format']))
	);
});

$app->match('/:bookmark', function (Turtle_Component_Http_Request $request, $bookmark, $app) {
	if (! $bookmark) {
		$app->abort(400, null, array('content_type' => $request->getMimeType($app['serializer.format'])));
	}

	try {
		$data = @$app['serializer']->getLoader($app['serializer.format'])->load($request->body);

		foreach ((array) $data as $k => $v) {
			$bookmark->$k = $v;
		}
		$bookmark->Owner = $app['user'];
		$bookmark->save();
	} catch (Exception $e) {
		$app->abort(
			400, 
			$app['serializer']->getDumper($app['serializer.format'])->dump(array('error' => $e->getMessage())), 
			array('content_type' => $request->getMimeType($app['serializer.format']))
		);
	}

	return $app->respond(
		$app['serializer']->getDumper($app['serializer.format'])->dump(array('bookmark' => $bookmark->toArray(false))),
		202,
		array('content_type' => $request->getMimeType($app['serializer.format']))
	);
})->assert('bookmark', '[a-zA-Z0-9]+')
->convert('bookmark', $bookmarkConverter)
->method('PUT', 'PATCH');

$app->delete('/:bookmark', function (Turtle_Application $app, Turtle_Component_Http_Request $request, $bookmark) {
	$headers = array('content_type' => $request->getMimeType($app['serializer.format']));

	if (! $bookmark) {
		$app->abort(404, null, $headers);
	}

	if ($bookmark->owner_id != $app['user']->id) {
		$app->abort(402, null, $headers);
	}

	$bookmark->delete();

	return $app->respond(null, 202, $headers);
})->assert('bookmark', '[a-zA-Z0-9]+')
->convert('bookmark', $bookmarkConverter);

/* YOU_CAN_FUNCTIONALY_TEST_THE_APP_USING_THIS_ROUTE */
$app->get('/test', function (Turtle_Application $app) {
	$client = new Turtle_Component_HttpClient_Client('http://localhost/api/web/index.php');
	$client->sync(false);

	// Try once at a time, comment 3 and use 1
	/* POST test */
// 	$client->doPost('/', array('X-Format' => 'json'), array(), json_encode(array(
// 	'name'        => 'Test client',
// 	'url'         => 'm.youtube.com',
// 	'description' => 'description test'
// )))->setCredentials('denis', '123456');

	/* PUT test */
	// $client->doPut('/9ef74df4f0e9eb739fca9ab19dde5f81', array('X-Format' => 'json'), array(), json_encode(array(
	// 	'name' => 'Updated name'
	// )))->setCredentials('denis', '123456');

	/* DELETE test */
	$client->doDelete('/850ac84e52661e1eec1f53bf1eeb53bf', array('X-Format' => 'json'))
		->setCredentials('denis', '123456');

	$contexts = $client->send();
	$context = $contexts[0];

	return '<html><head><script>'.file_get_contents(__DIR__.'/../vendor/buonaparte/turtle/src/Turtle/Extension/Debugger/highlight.js').'</script><script>hljs.initHighlightingOnLoad();</script><style>'.file_get_contents(__DIR__.'/../vendor/buonaparte/turtle/src/Turtle/Extension/Debugger/tomorrow.css').'</style><style>pre code {font-size:14px;}</style></head><body><pre><code class="php">'.var_export($context, true).'</code></pre></body></html>';
});
/* end YOU_CAN_FUNCTIONALY_TEST_THE_APP_USING_THIS_ROUTE */

return $app;