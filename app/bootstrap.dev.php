<?php

require __DIR__.'/../vendor/autoload.php';

$app = new Turtle_Application();

$app['autoloader']->registerPrefixes(array(
	/* autoload Doctrine bug */
	'sfYaml' => __DIR__.'/../vendor/doctrine/doctrine1/lib/Doctrine/Parser/sfYaml',
	/* PSR-0 application */
	'Api'    => __DIR__.'/../src'
));
$app['autoloader']->register();

$app->register(new Turtle_Extension_DoctrineExtension(), array(
	/* connections */
	'dbs.options' => array(
		'default'    => array(
			'conn'  	=> array(
				'dsn'            => 'mysql:host=localhost;dbname=api',
				'username'       => 'root',
				'password'       => ''
			),
			'autoconnect'                         => true,
			Doctrine_Core::ATTR_QUOTE_IDENTIFIER  => true,
			Doctrine_Core::ATTR_USE_DQL_CALLBACKS => true,
			Doctrine_Core::ATTR_VALIDATE          => Doctrine_Core::VALIDATE_ALL
		)
	),
	/* cli options */
	'db.cli.options' => array(
		/* models */
		'models_path'             => __DIR__.'/../src',
		'generate_models_options' => array(
			'baseClassPrefix'      => 'Api_Model_Base_',
			'baseClassesDirectory' => '',
			'classPrefix'          => 'Api_Model_',
			'classPrefixFiles'     => true,
			'pearStyle'            => true,
			'baseClassPrefix'      => 'Base_',
			'generateTableClasses' => true
		),
		/* migrations */
		'migrations_path'    => __DIR__.'/../bin/db/migrations',
		/* schemas */
		'yaml_schema_path'   => __DIR__.'/../bin/db/schemas',
		/* fixtures */
		'data_fixtures_path' => __DIR__.'/../bin/db/dumps'
	)
));

$app->register(new Api_Extension_HttpBasicAuthExtension());

$app->register(new Api_Extension_SerializerExtension(), array(
	'serializer.default_format' => 'xml'
));

return $app;