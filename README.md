# RAD rest-full API demo BOOKMARKING
## (based on Turtle - Component Frameword + Doctrine1)

Really simple proof of concept for University reasons.

### Install

Install via composer:
	
* git clone https://buonaparte@bitbucket.org/buonaparte/api.git
* php composer.phar install
* rename app/bootstrap.dev.php => app/bootstrap.php
* create your database schema: ./bin/turtle-doctrine migrate
* you will defenetly need a test user to play with it, try adding this middleware to the API:

		$app->before(function (Turtle_Application $app) {
			// specify a username and a password for the test user
			list($username, $password) = array('foobar', '123456');
			$em = $app['db.em']('Api_Model_User');
			if (! $user = $em->findOneByUsername($username)) {
				$user = $em->create();
			}
			$user->username = $username;
			// this will be hashed automaticly, as per Api_Model_User record implementation
			$user->password = $password;
			$user->first_name = 'Foo';
			$user->last_name = 'Bar';
			$user->email = 'foo@bar.org';
			$user->save();

			// now you are ready to go with your client
		});

### Usage

There only 4 routes defined in the Application:

	* **GET /** - list of 10 most recent bookmarks:

		* produses 200 Http Response status code when there is at least one bookmark, 204 (No-Content) otherwise 

	* **POST /** - creates a new bookmark for the User, based on Http Request body

		* The body format is negotiated using format=xml|json query string parameter or based on X-Format Http Request header (more correct will be using a voter implementation over Http-Accept headers family)
		* XML is the fallback format
		* 201 will be produced and the body will contain full resource as per serializer format negociated in case of success, 400 (Bad Request) with body containing serialized hash {error: "reason"} otherwise

	* **PUT|PATCH /:bookmark** - updates a bookmark, based on Http Request body, the bookmark is identified from the route token (basically a MD5 hash bookmark unique identifier)

		* For now the API implementation doent't really make difference betweeen PUT and PATCH (however is PATCH implementation as per Http RFC, because it does not require all bookmark fields on update)
		* 404 is returned in case there was not bookmark found for the User by the given route hash parameter
		* 202 status with Updated resource as body will be returned in case of success, 400 with {error: "reason"} serialized hash as body otherwise

	* **DELETE /:bookmark** - removes a bookmark, by the given hash

		* 202 (Accepted) will be returned is case of succes with **no body**, 404 otherwise.

3 Aditional Middlewares are included:

	* Authentication (based on plain, old, Basic HttpAuth) - can be replaced with any modern implementation, or using SSL will do the trick.
	* Authorization (just for demo, authorizes an User based on his status flag in the system)
	* Content format negociation phraze (please notice, "xml" will be used as fallback)


There is no client included, besids Turtle_Component_HtppClient Component, a more advanced example can be found in web/test.php or a highlighted version in web/app.php